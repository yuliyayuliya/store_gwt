package com.julia.store.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Dialog extends Composite {
    interface DialogUiBinder extends UiBinder<HTMLPanel, Dialog> {
    }

    private static DialogUiBinder ourUiBinder = GWT.create(DialogUiBinder.class);

    @UiField
    SuggestBox suggestBox;

    @UiField
    DivElement content;

    @UiField
    TextBox userName;

    @UiField
    TextBox dateInput;

    @UiField
    TextBox priceInput;

    @UiField
    TextBox countInput;

    @UiField
    Button confirmAddButton;

    @UiField
    Button cancelAddButton;

    @UiField
    DialogBox addItemDialog;

    @UiField
    DialogBox searchDialog;

    @UiField
    Button addButton;

    @UiField
    Button cancel;

    @UiField
    Button search;

    TextBox nameFieldContainer;

    @UiHandler("cancel")
    protected void onCancelButtonClick(ClickEvent event){
        searchDialog.hide();
    }
    @UiHandler("confirmAddButton")
    protected void onConfirmAddButtonClick(ClickEvent e) {
        if (!userName.getValue().isEmpty()) {
            addItem(userName.getValue(), dateInput.getValue(), countInput.getValue(), priceInput.getValue());

            userName.setValue("");
            priceInput.setValue("");
            dateInput.setValue("");
            countInput.setValue("");
        }
        addItemDialog.hide();

    }

    @UiHandler("cancelAddButton")
    protected void onCancelAddButtonClick(ClickEvent e) {
        addItemDialog.hide();
    }

    @UiHandler("addButton")
    protected void onAddButtonClick(ClickEvent e) {
        addItemDialog.center();
        addItemDialog.show();
    }

    @UiHandler("search")
    protected void onSearchButtonClick(ClickEvent e) {

        addItemDialog.center();
        searchDialog.show();
//        Должно быть клоузменю
        for (Item item : items) {
            if (item.getUser().equals(suggestBox.getText())) {
                searchDialog.show();
            }
        }
    }

    private List<Item> items = new ArrayList<>();

    public Dialog() {
        initWidget(ourUiBinder.createAndBindUi(this));
        addItemDialog.hide();

        Element dummy = DOM.getElementById("nameFieldContainer");
        nameFieldContainer = TextBox.wrap(dummy);

        nameFieldContainer.addChangeHandler(changeEvent -> {
           // обрабатываем
        });

        suggestBox.addValueChangeHandler(e ->
        {
            String value = e.getValue();
            for (Item item : items) {
                if (item.getUser().equals(value)) {
                    dateInput.setValue(new Date().toString());
                    priceInput.setValue(item.getProduct());
                    userName.setValue(item.getUser());
                    countInput.setValue(item.getAmount());
                    addItemDialog.show();
                    break;
                }
            }
        });

    }

    private void addItem(String user, String date, String count, String product) {
        Item item = new Item();
        item.setUser(user);
        item.setDate(date);
        item.setAmount(count);
        item.setProduct(product);


        MultiWordSuggestOracle oracle = (MultiWordSuggestOracle) suggestBox.getSuggestOracle();
        oracle.add(user);

        content.appendChild(item.getElement());
        items.add(item);
    }


}