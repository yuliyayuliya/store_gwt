package com.julia.store.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Element;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;


public class Item {
    private final DivElement element;

    interface ItemUiBinder extends UiBinder<DivElement, Item> {
    }

    private static ItemUiBinder ourUiBinder = GWT.create(ItemUiBinder.class);

    @UiField
    Element orderId;
    @UiField
    Element user;
    @UiField
    Element product;
    @UiField
    Element date;
    @UiField
    Element amount;


    public Item() {
        element = ourUiBinder.createAndBindUi(this);
    }


    public String getOrderId() {
        return orderId.getInnerText();
    }

    public void setOrderId(int id) {
        orderId.setInnerText(String.valueOf(id));
    }

    public String getUser() {
        return user.getInnerText();
    }

    public void setUser(String s) {
        user.setInnerText(s);
    }

    public String getDate() {
        return date.getInnerText();
    }

    public void setDate(String s) {
        date.setInnerText(s);
    }

    public String getAmount() {
        return amount.getInnerText();
    }

    public void setAmount(String s) {
        amount.setInnerText(s);
    }

    public String getProduct() {
        return product.getInnerText();
    }

    public void setProduct(String s) {
        product.setInnerText(s);
    }

//    public boolean isDone() {
//        return done.getActive();
//    }
//
//    public void setDone(boolean b) {
//        done.setActive(b);
//    }

    public DivElement getElement() {
        return element;
    }
}
